

# A small utility function for retrieving existing values by key from values
# already parsed, i.e.: allows for easy setting of default values from other,
# already parsed options
def other(key):
    def func(values):
        if key in values:
            return values[key]
        else:
            return None
    return func
