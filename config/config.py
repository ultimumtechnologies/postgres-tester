class ConfigException(Exception):
    pass


class ConfigParser:
    def __init__(self, parsers=[], options={}):
        self.parsers = parsers
        self.options = options

        self.values = {}

    def _get_definition(self, option, index):
        definitions = option['definitions']

        if index >= len(definitions):
            return None
        else:
            return definitions[index]

    def run(self):
        self.values = {
            name: None for name in self.options.keys()
        }

        for index, parser in enumerate(self.parsers):
            for name, option in self.options.items():
                # If definition for a given parser exists, pass it to the
                # parser
                definition = self._get_definition(option, index)
                if definition:
                    parser.add_definition(definition,
                                          name,
                                          option['description'],
                                          option['structure'])

            # Run the parser
            parser.parse(self.values)

            # Get values from parser - EARLIER parser values have priority
            # over LATER - this allows command line arguments to be parsed
            # first, effect later parsers (--c to ini), but still have
            # precedence over any parsed values (i.e.: Values on the command
            # line should generally have priority even though it's parsed
            # first)
            for name, option in self.options.items():
                if self._get_definition(option, index):
                    value = parser.get_value(name)
                    if self.values[name] is None and value is not None:
                        self.values[name] = value

        # default values are set post parsing in a separate cycle, because
        # they may reference different values, which need to be set at this
        # point to be correctly usable
        #
        # todo: add a secondary dictionary for sources => will allow an easier
        #       to debug output, e.g.:
        #           {
        #               "name": "<value, source, type>"
        #           }
        #           ...
        #           {   "option": "<'/etc/config,ini', default, path>" }
        for name, option in self.options.items():
            if not self.values[name] and 'default' in option:
                default_value = option['default']
                if callable(default_value):
                    self.values[name] = default_value(self.values)
                else:
                    self.values[name] = default_value

        self.validate()

    def validate(self):
        missing_values = []

        for name, option in self.options.items():
            if option['required'] and not self.values[name]:
                missing_values.append(name)

        if missing_values:
            raise ConfigException(f"Missing required options: {missing_values}")

    def get_value(self, name):
        return self.values[name]
