from dataclasses import dataclass

import configparser
import os


@dataclass
class Setting:
    section: str
    name: str


class IniParser:
    def __init__(self, default_path):
        self.default_path = default_path
        self.parser = configparser.ConfigParser()

        self.definitions = {}

    def add_definition(self, definition, name, description, structure):
        self.definitions[name] = definition, structure

    def parse(self, config: dict):
        path = config.get('config_path') or self.default_path

        if os.path.isfile(path):
            self.parser.read(path)

    def get_value(self, name):
        definition, structure = self.definitions.get(name)

        if structure == str:
            return self.parser.get(
                definition.section, definition.name, fallback=None
            )

        # todo: different data types parsing
        #   self.parser.getboolean()
        #   self.parser.getfloat()
        #   self.parser.getint()
        #   self._parse_url
