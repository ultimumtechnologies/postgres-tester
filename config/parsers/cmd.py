import argparse
from dataclasses import dataclass


@dataclass
class Parameter:
    parameter: str = None
    has_value: bool = True


class CmdParser:
    def __init__(self):
        self.definitions = []
        self.values = {}

    def add_definition(self, definition, name, description, structure):
        self.definitions.append(
            (definition.parameter, definition.has_value, name, description,
             structure)
        )

    def parse(self, config):
        parser = argparse.ArgumentParser()

        for parameter, has_value, name, description, structure \
                in self.definitions:
            # todo: separate structure and type
            '''            
            if structure == list:
                parser.add_argument(
                    parameter, help=description, type=str, action='append'
                )
            '''

            # named or flagged arguments (--name, -flag)
            if parameter:
                # value parameter
                if has_value:
                    parser.add_argument(
                        parameter, help=description, type=structure, dest=name
                    )
                # present/not present | true/false switch
                else:
                    parser.add_argument(
                        parameter, help=description, dest=name,
                        action='store_const', const=True
                    )
            # positional argument
            else:
                parser.add_argument(
                    name, help=description, type=structure, nargs='?'
                )
        self.values = parser.parse_args()

    def get_value(self, name):
        return getattr(self.values, name)
