import os
from dataclasses import dataclass


@dataclass
class Variable:
    name: str = None


class EnvParser:
    def __init__(self):
        self.definitions = []
        self.values = {}

    def add_definition(self, definition, name, description, structure):
        self.definitions.append(
            (definition.name, name, structure)
        )

    def parse(self, config):
        for def_name, name, structure in self.definitions:
            value = os.getenv(def_name)

            if value:
                if structure == bool:
                    converted_value = value.lower() == 'true'
                else:
                    converted_value = value

                self.values[name] = converted_value

    def get_value(self, key):
        return self.values.get(key)
