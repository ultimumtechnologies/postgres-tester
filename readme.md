# Read Me

## Environment setup
To run either of the apps, the following environmental
variables have to be set:

* PTT_HOST: postgres database host
* PTT_DATABASE: db name
* PTT_USER: postgres username
* PTT_PASS: postgres user password


## Program usage
The program is usable as a standalone application from the source code or
in a docker container using the image 
dockerhub.ultimum.io:443/kubernetes/psql_web.

Current version is v1.0.0-alpha1.

### Web interface
The web server provides a ui for testing database connections to a PostGRES 
database. It operates as an SPA served on port 5000.

---
**WARNING**

SQL inputs are currently not sanitized before db insertionl; **Be _EXTREMELY_ 
careful when using manual data insertion!**

---

Commandline:

```shell
> python3 run_web.py
```

Docker container:
```shell
>  sudo docker run -ti -e PTT_HOST=<postgres_host> -e PTT_DATABASE=<database> -e PTT_USER=<user> -e PTT_PASS=<pass> dockerhub.ultimum.io:443/kubernetes/psql_web:v1.0.0-alpha1
```


### Data generator
The data generator is a mode of operation which generates random data 
and stores them in the provided database once every five seconds.

---
**NOTE**

The standalone data generator is deprecated as of v1.0.0-alpha1. Data generation
has been moved to the web interface.

---

Commandline:

```shell
> python3 run_generator.py
```

Docker container:

```shell
>  sudo docker run -ti -e PTT_HOST=<postgres_host> -e PTT_DATABASE=<database> -e PTT_USER=<user> -e PTT_PASS=<pass> dockerhub.ultimum.io:443/kubernetes/psql_web:v1.0.0-alpha1 python3 run_generator.py
```


## TODO
1. Data SQL sanitization before insertion 
1. The uWsgi script is currently untested and probably not working.