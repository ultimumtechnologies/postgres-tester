import time

import psycopg2

from database.exceptions import DriverException
from logs import factory
LOG = factory.build_logger(__name__)


class PostgresDriver:
    # todo: replace prints with and add logging where appropriate
    # todo: driver retries on failure
    def __init__(self, host, database, user, password,
                 retries=10, retry_sleep=3):
        self.host = host
        self.database = database
        self.user = user
        self.password = password

        self.retries = retries
        self.retry_sleep = retry_sleep

    def _execute(self, query, retry_no):
        connection = None
        cur = None
        try:
            LOG.debug(f"Executing query: {query}")
            connection = psycopg2.connect(
                host=self.host,
                database=self.database,
                user=self.user,
                password=self.password
            )
            LOG.info(f"Established connection to DB on try {retry_no+1} of "
                     f"{self.retries}")
            cur = connection.cursor()
            result = cur.execute(query)
            return result, cur, connection
        except (psycopg2.DatabaseError, psycopg2.OperationalError) as error:
            if retry_no < self.retries:
                LOG.info(
                    f"Failed to execute query on try {retry_no+1} of "
                    f"{self.retries}. "
                    f"Retrying in {self.retry_sleep} seconds. "
                    f"Reason: {str(error)}")
                time.sleep(self.retry_sleep)
                return self._execute(query, retry_no+1)
            else:
                LOG.error(
                    f"Failed to execute query on try {retry_no}. Retry limit "
                    f"exceeded")
                if connection is not None:
                    connection.close()
                if cur is not None:
                    cur.close()
                raise DriverException(error)

    def _read_query(self, query):
        result, cur, connection = self._execute(query, 0)

        res = cur.fetchall()
        cur.close()
        connection.close()
        return res

    def _read_one_query(self, query):
        result, cur, connection = self._execute(query, 0)

        res = cur.fetchone()
        cur.close()
        connection.close()
        return res

    def _write_query(self, query):
        result, cur, connection = self._execute(query, 0)

        cur.close()
        connection.commit()
        connection.close()

        return result

    @property
    def connected(self):
        return self.connection is not None

    def get_table(self, table_name):
        query = f"SELECT * " \
                f"FROM pg_catalog.pg_tables " \
                f"WHERE tablename = '{table_name}'"
        return self._read_one_query(query)

    def create_table(self, table_name, column_defs=[]):
        # if not column_defs:
        #    todo: raise critical
        #          or
        #          wrap the join into a try and raise critical on failure
        columns = ",".join(column_defs)
        # if not columns
        #   todo: raise critical
        query = f"CREATE TABLE IF NOT EXISTS {table_name} (" \
                f"{columns}" \
                f")"
        return self._write_query(query)

    def drop_table(self, table_name):
        query = f"DROP TABLE IF EXISTS {table_name}"
        return self._write_query(query)

    def insert(self, table_name, value):
        columns = ",".join(value.keys())
        values = ",".join(value.values())

        query = f"INSERT INTO {table_name} ({columns}) " \
                f"VALUES ({values})"

        return self._write_query(query)

    # todo: add WHERE conditions with checks
    def get(self, table_name, columns, order_by=None, order='DESC', limit=None):
        column_names = ",".join(columns)

        query_elements = [
            "SELECT", column_names, "FROM", table_name
        ]
        if order_by:
            query_elements.append("ORDER BY")
            query_elements.append(order_by)
            query_elements.append(order)
        if limit:
            query_elements.append("LIMIT")
            query_elements.append(str(limit))

        query = " ".join(query_elements)

        return self._read_query(query)

    # todo: add conditions
    def count(self, table_name):
        query = f"SELECT COUNT(*) FROM {table_name}"
        result = self._read_query(query)
        return result[0][0]

    def version(self):
        query = 'SELECT version()'
        return self._read_one_query(query)
