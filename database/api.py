import random
from datetime import datetime


from logs import factory
LOG = factory.build_logger(__name__)


class DAO:
    # todo: object based table and data definitions and manipulation
    table_name: str = 'filebeat_test'
    columns: list = [
        'id serial PRIMARY KEY',
        'created TIMESTAMP NOT NULL',
        'content TEXT'
    ]

    def __init__(self, driver):
        self.driver = driver

    # Initializes the db connection, table and get database information
    def initialize(self):
        LOG.debug(f"Initializing driver.")
        version = self.driver.version()
        LOG.info(f"Database connection OK, info: {version}")
        LOG.debug(f"Checking table presence ({self.table_name})")

        table = self.driver.get_table(self.table_name)
        if table:
            LOG.info(f"Table found. {self.table_name}")
        else:
            LOG.info(f"Table not found, creating. ({self.table_name})")
            # todo: catch and raise critical
            self.driver.create_table(self.table_name, self.columns)
            LOG.info(f"Table created successfully. ({self.table_name})")

        LOG.debug("Polling existing data.")
        data_count = self.driver.count(self.table_name)
        LOG.info(f"Table currently has {data_count} entries. "
                 f"({self.table_name})")

    def _destroy_table(self):
        self.driver.drop_table(self.table_name)

    def generate(self):
        self.create(str(random.getrandbits(128)))

    def get_all(self):
        # todo: columns defined in a dictionary
        return self.driver.get(self.table_name, ['*'])

    def get_number(self, number):
        return self.driver.get(
            self.table_name, ['*'], order_by='created', limit=number
        )

    def create(self, content):
        timestamp = datetime.now()

        # todo: TO_TIMESTAMP shouldn't be set here -the DAO doesn't speak sql
        #       - model to be developed
        to_timestamp = f"TO_TIMESTAMP('{str(timestamp)}'," \
                       f"'YYYY-MM-DD HH24:MI:SS.US')"

        value = {
            'created': to_timestamp,
            'content': f"'{str(content)}'"
        }
        self.driver.insert(self.table_name, value)
