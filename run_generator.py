import time

from common import build_postgres_dao

if __name__ == "__main__":
    dao = build_postgres_dao()
    dao.initialize()

    while True:
        dao.generate()
        time.sleep(5)

