from flask import Flask, render_template, jsonify, request

from common import build_postgres_dao
from database.exceptions import DriverException


from logs import factory
LOG = factory.build_logger(__name__)

app = Flask(__name__)


# We currently aren't catching exceptions here, because failure to initialize
# currently ends in termination anyways.
dao = build_postgres_dao()
dao.initialize()


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/data', methods = ['GET', 'POST'])
def data():
    try:
        if request.method == "GET":
            data = dao.get_number(15)
            res = [{
                'id': datum[0],
                'timestamp': datum[1],
                'content': datum[2]
            } for datum in data]

            return jsonify(res)
        elif request.method == "POST":
            data = request.form
            value = data['value']
            dao.create(value)
            return jsonify({})
    except DriverException:
        return jsonify({}), 500


def serve():
    app.run(host="0.0.0.0")
