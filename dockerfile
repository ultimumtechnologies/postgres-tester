FROM python:3.8-buster

WORKDIR ~
COPY ./ ./
RUN pip3 install --no-cache-dir -r web_requirements.txt
CMD ["python3", "run_web.py"]