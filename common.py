from config import ConfigParser
from config.parsers import EnvParser
from config.parsers.env import Variable as EnvVariable
from database.api import DAO
from database.drivers import PostgresDriver


options = {
    'host': {
        'definitions': (
            EnvVariable('PTT_HOST'),
        ),
        'structure': 'str',
        'required': True,
        'description': 'POSTGRES host'
    },
    'database': {
        'definitions': (
            EnvVariable('PTT_DATABASE'),
        ),
        'structure': 'str',
        'required': True,
        'description': 'POSTGRES database name'
    },
    'user': {
        'definitions': (
            EnvVariable('PTT_USER'),
        ),
        'structure': 'str',
        'required': True,
        'description': 'POSTGRES database user name'
    },
    'password': {
        'definitions': (
            EnvVariable('PTT_PASS'),
        ),
        'structure': 'str',
        'required': True,
        'description': 'POSTGRES database password'
    }
}


def build_postgres_dao():
    # Read configuration from environment
    config = ConfigParser(
        parsers=(
            EnvParser(),
        ),
        options=options
    )
    config.run()

    # Initialize the db driver
    driver = PostgresDriver(
        config.get_value('host'),
        config.get_value('database'),
        config.get_value('user'),
        config.get_value('password')
    )

    # Create the DAO
    dao = DAO(driver)

    return dao
